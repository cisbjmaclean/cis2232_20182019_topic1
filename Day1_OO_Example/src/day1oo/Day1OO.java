package day1oo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Day1OO {

    public static void main(String[] args) throws IOException {

        //Call method which has some file input/output.
        doFileTesting();
        boolean error = false;
        int userEntry = 0;
        Scanner input = new Scanner(System.in);
        do {
            error = false;
            try {
                System.out.println("Enter 1 for cylinder 2 for cube or 0 to exit:");
                userEntry = input.nextInt();
                input.nextLine();
                Container container = null;
                switch (userEntry) {
                    case 1:
                        container = new ContainerCylinder();
                        break;
                    case 2:
                        container = new ContainerCube();
                        break;
                    case 0:
                        System.out.println("Goodbye");
                        break;
                    default:
                        System.out.println("Error");
                }
                if (userEntry > 0) {
                    container.getInputs();
                    container.display();
                }
            } catch (Exception e) {
                error = true;
                System.out.println("Error encountered");
            }
        } while (userEntry > 0 && !error);

    }

    public static void doFileTesting() throws IOException {

        
        Path path = Paths.get("c:\\temp\\testfile.txt");
        System.out.println("getNameCount=" + path.getNameCount());

        //Create the directory that will contain the file (in case it does not exist)
        Files.createDirectories(Paths.get("c:\\temp"));

        //Also write to the file when a new camper was added!!
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter("c:\\temp\\testfile.txt", false);
            bw = new BufferedWriter(fw);
            bw.write("TE" + System.lineSeparator());
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

        ArrayList<String> lines = (ArrayList<String>) Files.readAllLines(path);

        for (String current : lines) {
            System.out.println(current);
        }

    }

}
