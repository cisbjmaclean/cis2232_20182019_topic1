package day1oo;

import java.util.Scanner;

/**
 * Class to represent process of filling a cylinder container with water.
 *
 * @author bjmaclean
 * @since 20180907
 */
public class ContainerCube extends Container  {

    private double height;

    public void getInputs() {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter height (inches)");
        height = input.nextDouble();
        input.nextLine();

        System.out.println("Please enter flow rate (seconds/litre)");
        flowRate = input.nextDouble();
        input.nextLine();

    }

    /**
     * This will return the time to fill based on the attributes of this object.
     *
     * @since 20180907
     * @author BJM
     * @return time
     */
    public double getTime() {
        //convert height to m
        double height = this.height * CM_IN_INCH / 100;

        //volume in litres h*h*h*1000
        volume =  height* height *height;

        //time equals number of litres * seconds/litre
        double time = volume * flowRate;

        return time;
    }

    public void display() {
        System.out.println(this.toString());
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }



    @Override
    public String toString() {
        return "Height=" + height + "\nFlowRate="
                + flowRate + "\nTime to fill=" + getTime();
    }
}
