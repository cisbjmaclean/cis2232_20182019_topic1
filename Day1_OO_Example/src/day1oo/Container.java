package day1oo;

/**
 *
 * @author bjmaclean
 */
public abstract class Container implements Fillable{

    protected double volume, flowRate;
    
    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }
    
        public double getFlowRate() {
        return flowRate;
    }

    public void setFlowRate(double flowRate) {
        this.flowRate = flowRate;
    }
    
}
