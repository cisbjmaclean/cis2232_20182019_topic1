package day1oo;

/**
 *
 * @author bjmaclean
 */
public interface Fillable {

        public static final double CM_IN_INCH = 2.54;
        public abstract void getInputs();
        public abstract double getTime();
        public abstract void display();
    
}
