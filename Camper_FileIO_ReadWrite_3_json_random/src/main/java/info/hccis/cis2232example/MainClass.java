package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/cis2232/campersRandom.json";

    public static void main(String[] args) throws IOException {
        //Create a file
        Files.createDirectories(Paths.get("/cis2232"));
        
        //Adding sample code for reading/writing objects to file.
        //Testing writing/reading objects.
        //Write an object to file
        CamperFileUtility cfu = new CamperFileUtility(("/cis2232/campersRandom.ser"));
        Camper test = new Camper();
        test.setFirstName("BJ");
        cfu.WriteObjectToFile(test);
        
        Camper testBack = new Camper();
        testBack = (Camper) cfu.readObjectFromFile();
        Object testObject = testBack;
        
        System.out.println(testBack);
        System.out.println(testObject);
        
        //End of sample code.
        
        
        Path path = Paths.get(FILE_NAME);

        //Only create and write 10000 spaces if the file does not already exist.
        RandomAccessFile raf;
        if (Files.notExists(path)) {
            raf = new RandomAccessFile(FILE_NAME, "rw");
            String tenSpaces = "          ";
            for (int count = 1; count <= 1000; count++) {
                raf.writeBytes(tenSpaces);
            }
        } else {
            raf = new RandomAccessFile(FILE_NAME, "rw");
        }

        //testing out some functionality associated with random access files.
//        raf.seek(10);
//        byte[] test = new byte[5];
//        raf.read(test);
//        System.out.println("read these bytes***" + test + "***");
//        String fiveCharsThatWereRead = new String(test);
//        System.out.println("REad=" + fiveCharsThatWereRead);
//        System.exit(0);
        ArrayList<Camper> theList = new ArrayList();
        loadCampers(theList, raf);
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    theList.set((newCamper.getRegistrationId() - 1), newCamper);
                    CamperFileUtility.writeCamperToRandomFile(raf, newCamper);

                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for (Camper camper : theList) {
                        if (camper.getRegistrationId() > 0) {
                            System.out.println(camper);
                        }
            }
            break;
          case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        
    }

    while (!option.equalsIgnoreCase (

"x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers, RandomAccessFile raf) throws IOException {
        System.out.println("Loading campers from random accessfile");
        int countOfCampers = 0;
        for (int count = 1; count <= 100; count++) {
            //St
            int startLocation = (count - 1) * 100;
            raf.seek(startLocation);
            byte[] camperBytes = new byte[100];
            raf.read(camperBytes);
            String camperJson = new String(camperBytes);

            camperJson = camperJson.trim();
            Camper temp;
            if (camperJson.length() > 0) {
                //Get a camper from the string
                Gson gson = new Gson();
                temp 

= gson.fromJson(camperJson, Camper.class
);
            }else{
                //Create a new camper
                temp = new Camper(); 
            }
            campers.add(temp);

        }

//        try {
//            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));
//
//            for (String current : test) {
//                System.out.println("Loading:  " + current);
//                //Get a camper from the string
//                Gson gson = new Gson();
//                Camper temp = gson.fromJson(current, Camper.class);
//                campers.add(temp);
//                if (temp.getRegistrationId() > Camper.getMaxRegistrationId()) {
//                    Camper.setMaxRegistrationId(temp.getRegistrationId());
//                }
//                count++;
//            }
//
//        } catch (IOException ex) {
//            System.out.println("Error loading campers from file.");
//            System.out.println(ex.getMessage());
//        }
        System.out.println("Finished...Loading campers from file (Loaded " + countOfCampers + " campers)\n\n");

    }
}
